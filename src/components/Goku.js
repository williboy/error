import React, { Component } from 'react';
import handleClicks from './HandleClicks';
import goku from './goku.jpg';

class Goku extends Component {

    render() {

        const { background, clickHandler } = this.props;
        return (
            <div className={`col ${ background }`}>
                <img onClick={clickHandler} src={goku} alt="goku" /><br />
                
            </div>
        )
    }
}

export default handleClicks(Goku);
