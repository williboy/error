import React from 'react';



class ErrorBoundary extends React.Component {
   constructor(props) {
     super(props);
     this.state = { hasError: false };
   }
  
    static getDerivedStateFromError(error) {
      // Mettez a jour l'etat, de facon a monter l'UI de repli au prochain rendu.
      return { hasError: true 
      };
    }
  
    render() {
      if (this.state.hasError) {
       // Vous pouvez afficher n'importe quelle  UI de repli.
       return (
        <div className="col bg-danger">
        <p style={{color: '#ffffff'}}>Oups, nous avons un probleme!</p>
        
    </div>
       )
      }
  
     return this.props.children;
    }
  }

  export default ErrorBoundary;



