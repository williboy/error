import React, { Component } from 'react';
import handleClicks from './HandleClicks';
import vegeta from "./vegeta.jpg";

 class Vegeta extends Component {

    render() {

        const { background, clickHandler } = this.props;
        return (
            <div className={`col ${background}`}>
                <img onClick={clickHandler} src={vegeta} alt="vegeta" /><br />
                
            </div>
        )
    }
}

export default handleClicks(Vegeta);
